# Wmbusmeters addon
Viz https://github.com/weetmuts/wmbusmeters

## Home assistant 
Add sensor
```
  - platform: mqtt
    name: "Hot watter"
    state_topic: "metter/02798563"
    value_template: '{{ value_json.total_m3 }}'
    unit_of_measurement: "m3"  
```
addon config example

```
log_level: info
log_telegraph: false
mqtt:
  server_prefix: metter
  username: **
  password: **
  port: 1883
  url: 192.168.1.11
devices:
  - name: hotWatter
    type: 'apator162:t1'
    id: '02711111'
    key: '00000000000000000000000000000000'
```