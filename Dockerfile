FROM weetmuts/wmbusmeters
COPY docker-entrypoint.sh /wmbusmeters/docker-entrypoint.sh
CMD ["sh", "/wmbusmeters/docker-entrypoint.sh"]