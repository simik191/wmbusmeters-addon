#!/bin/sh

[ ! -d /wmbusmeters_data/logs/meter_readings ] && mkdir -p /wmbusmeters_data/logs/meter_readings
[ ! -d /wmbusmeters_data/etc/wmbusmeters.d ] && mkdir -p /wmbusmeters_data/etc/wmbusmeters.d

CONFIG_PATH=/data/options.json

PREFIX=$(cat $CONFIG_PATH  | jq --raw-output '.mqtt.server_prefix')
ADDRESS=$(cat $CONFIG_PATH  | jq --raw-output '.mqtt.url')
PORT=$(cat $CONFIG_PATH  | jq --raw-output '.mqtt.port')
PASS=$(cat $CONFIG_PATH  | jq --raw-output '.mqtt.password')
USER=$(cat $CONFIG_PATH  | jq --raw-output '.mqtt.username')
LOG_TELEGRAM=$(cat $CONFIG_PATH  | jq --raw-output '.log_telegraph')
LOG_LEVEL=$(cat $CONFIG_PATH  | jq --raw-output '.log_level')


echo "
loglevel=$LOG_LEVEL
device=auto
listento=t1
logtelegrams=$LOG_TELEGRAM
format=json
shell=/usr/bin/mosquitto_pub -h $ADDRESS -p $PORT -P $PASS -u $USER -t $PREFIX/\"\$METER_ID\" -m \"\$METER_JSON\"
" > /wmbusmeters_data/etc/wmbusmeters.conf

jq -c '.devices| .[]' $CONFIG_PATH | while read i; do
  echo "--$i--"
   name=$(echo $i  | jq --raw-output '.name')
  type=$(echo $i  | jq --raw-output '.type')
  id=$(echo $i  | jq --raw-output '.id')
  key=$(echo $i  | jq --raw-output '.key')
     echo -e "name=$name\ntype=$type\nid=$id\nkey=$key" > /wmbusmeters_data/etc/wmbusmeters.d/"$name"
done

/wmbusmeters/wmbusmeters --useconfig=/wmbusmeters_data